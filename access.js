//Make Sure you put the access_token at the end of the query string

// Imports
const request = require("request");
const querystring = require("querystring");
const dotenv = require('dotenv');
dotenv.config();


function initialize() {

    var options = {
        method: "POST",
        url: "https://api.ipdatatel.com/oauth/token",
        headers: { "content-type": "application/x-www-form-urlencoded" },
        body: querystring.stringify({
            grant_type: "password",
            username: process.env.USERNAME,
            password: process.env.PASSWORD,
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLIENT_SECRET
        })
    };

    return new Promise((resolve, reject) => {
        request.post(options, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })
}

var tokenInit = initialize();

tokenInit.then((result) => {
    console.log("Getting Access_Token!");
    process.env.ACCESS_TOKEN = result.access_token;
    process.env.REFRESH_TOKEN = result.refresh_token;
    console.log("Finished!");
    console.log(`Your Access Token is: ${process.env.ACCESS_TOKEN}`);
    console.log(`Your Refresh Token is : ${process.env.REFRESH_TOKEN}`);
});
