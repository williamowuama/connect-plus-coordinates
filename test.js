const WebSocket = require("ws");
const request = require("request");
const rp = require("request-promise");
const querystring = require("querystring");
const dotenv = require('dotenv');
const fs = require("fs");
dotenv.config();


function initialize() {

    var options = {
        method: "POST",
        url: "https://api-dev.alula.net/oauth/token",
        headers: { "content-type": "application/x-www-form-urlencoded" },
        body: querystring.stringify({
            grant_type: "password",
            username: process.env.USERNAME,
            password: "quixotic chicken",
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLIENT_SECRET
        })
    };

    return new Promise((resolve, reject) => {
        request.post(options, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })
}

var tokenInit = initialize();

tokenInit.then((result) => {
    console.log("Getting Access_Token!");
    process.env.ACCESS_TOKEN = result.access_token;
    process.env.REFRESH_TOKEN = result.refresh_token;
    console.log("Finished!");
    console.log(`Your Access Token is: ${process.env.ACCESS_TOKEN}`);
})
.then(() => {
    var lon;
    var lat;
    var file = fs.createWriteStream('test.txt');
    const ws = new WebSocket(`wss://api-dev.alula.net/ws/v1?access_token=${process.env.ACCESS_TOKEN}`);

    var array = fs.readFileSync("file.txt").toString().split("\n");
   // console.log(array);

    // for(i in array){
    //     console.log(array[i]);
    // };

    id = [
        "002dad7c-0359-4c86-aa66-48952712a452",
        "035019f4-7fb9-4657-a78a-17d7c5e260c8",
        "049006ff-8135-488f-acb5-7c85ca0d8f2d",
        "06218170-65e7-11e8-aa97-b9832d78e20d",
        "06936e40-d9db-11e7-80ba-51e0c12cdc17",
        "0704a8f0-8f7d-11e8-bf0d-c9768ad3445b",
        "076bd6f0-8600-11e8-b010-15e9f201cf6b",
        "0958da0a-f4c5-4281-a965-895a4d82556f",
        "09deaec0-9f9d-11e7-a30d-cb2384cdfd3c",
        "0a91c6a0-e5b0-11e7-9631-f3a7a5db53d0"
    ];

        ws.on('open', function open() {
            for(i in id) {
                setTimeout(() => {
                    console.log("SUBSCRIBING");
                    ws.send(`{\"channel\": \"device.helix\", \"subscribe\": {\"deviceId\": \"${id[i]}\"}}`);
                    setTimeout(() => {
                        console.log("GETTING LOCATION");
                        ws.send(`{\"channel\": \"device.helix\", \"send\": {\"deviceId\": \"${id[i]}\", \"cmdrsp\": \"requestMfd\", \"payload\": [{\"name\": \"locationCoordinates\"}]}}`);
                    }, 1000)
                    // setTimeout(() => {
                    //     console.log("UN-SUBSCRIBING");
                    //     ws.send(`{\"channel\": \"device.helix\", \"unsubscribe\": {\"consumerTag\": \"${process.env.CONSUMER_TAG}\"}}`);
                    //     ws.close();
                    // }, 5000)
                }, 1000);
            };


        ws.on('message', function incoming(data) {
            if (data){
                var d = JSON.parse(data);
                //console.log("You got some data bro\n");
                //console.log(d);
            } else {
                console.log("You aint get no data son!");
            }

            // try {
            //     if (d.subscribe.consumerTag){
            //         process.env.CONSUMER_TAG = d.subscribe.consumerTag;
            //         console.log("You got a consumer tag becuause you subscribed");
            //     }
            //     process.env.CONSUMER_TAG = d.subscribe.consumerTag;
            // } catch (error) {
            //    // console.log(error);
            // }

            try {
                if (d.event.data.payload) {
                    console.log(d.event.d.payload)
                    lat = d.event.data.payload[0].value.latitude;
                    lon = d.event.data.payload[0].value.longitude;
                    console.log(`lat: ${lat} lon:${lon}`);
                    //console.log("Got Location");
                    file.write(lat + ", " + lon + "\n");
                } else {
                    //console.log("It doesn't work");
                }
                //console.log(`lat: ${lat} lon:${lon}`);
                // file.write(lat + ", " + lon + "\n");
            } catch (error) {
                //console.log(error);
            }

        });
    });
    
    setTimeout(() => {
        ws.close();
        console.log("closing file");
        file.end();
    }, 30000);
})
// .then(() => {
//     var https = require("https");
//     url = `https://api-dev.alula.net/rest/v1/devices?filter[programId][$like]=32&customOptions[omitRelationships]=true&access_token=${process.env.ACCESS_TOKEN}`;
//     //console.log(url);
//     var request = https.get(url, function(response){
//         var buffer = "",
//         data;

//         response.on("data", function(chunk){
//             buffer += chunk;
//         });
//         response.on("end", function(err){
//             data = JSON.parse(buffer);
//             console.log(data);
//         });
//     })
// })
.catch((err) => {
    console.log("Something went wrong");
    console.log(err);
});

