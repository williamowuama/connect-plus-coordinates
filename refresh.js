// Make Sure you put the access_token at the end of the query string

const request = require("request");
const querystring = require("querystring");
const dotenv = require('dotenv');
dotenv.config();

function refresh(){

    var options = {
        method: "POST",
        url: "https://api.ipdatatel.com/oauth/token",
        headers: { "content-type": "application/x-www-form-urlencoded" },
        body: querystring.stringify({
            grant_type: "refresh_token",
            client_id = process.env.CLIENT_ID,
            client_secret = process.env.CLIENT_SECRET,
            refresh_token = process.env.REFRESH_TOKEN
        })
    };

    return new Promise((resolve, reject) => {
        request.post(options, (error, response, body) => {
            if(error){
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}


var tokenRefresh = refresh();

tokenRefresh.then((results) => {
    console.log("Preparing to Refresh Tokens");
    console.log(`Using Refresh Token : ${process.env.REFRESH_TOKEN}`);
    process.env.ACCESS_TOKEN = results.access_token;
    process.env.REFRESH_TOKEN = results.refresh_token;
    console.log("Finished!");
    console.log(`Your new Access Token is : ${process.env.ACCESS_TOKEN}`);
    console.log(`Your new Refresh Token is : ${process.env.REFRESH_TOKEN}`);
});