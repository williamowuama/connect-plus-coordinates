'use strict';

const WebSocket = require('ws'),
    EventEmitter = require('events'),
    rp = require('request-promise'),
    uuid = require('uuid'),
    debug = require('debug')('alula-api-client:index'),
    StateMachine = require('javascript-state-machine'),
    _ = require('underscore');

var Promise = require('bluebird');

class AlulaRPC {
    constructor(client, opts) {
        this.client = client;
    }

    call(method, params) {
        return this.client.send('rpc.tunnel', {
            request: {
                id: uuid.v1(),
                method,
                params
            }
        });
    }
}

class AlulaAPIClient extends EventEmitter {
    /**
     * @brief Initialize API client
     *
     * @param opts
     * @param opts.path String; WS URI
     * @param opts.authPath String; OAuth URI
     * @param opts.connectTimeout Number (ms)
     * @param opts.reconnectTimeout Number (ms)
     * @param opts.responseTimeout Number (ms)
     * @param opts.clientId String; OAuth client ID
     * @param opts.clientSecret String; OAuth client secret
     * @param opts.username String; OAuth username (password grant)
     * @param opts.password String; OAuth password (password grant)
     * @param opts.disableTokenManagement Boolean; Disable token management
     * @param opts.pingTimeout Number (ms)
     *
     * Events:
     *  incoming
     *  error
     *  open
     *  close
     */
    constructor(opts) {
        super();

        this.setMaxListeners(0); // No limit

        if (!opts) {
            opts = {};
        }

        this.opts = {
            path: opts.path || 'wss://api.ipdatatel.com/ws/v1',
            authPath: opts.authPath || 'https://api.ipdatatel.com/oauth',
            connectTimeout: opts.connectTimeout || 10000,
            reconnectTimeout: opts.reconnectTimeout || 5000,
            responseTimeout: opts.responseTimeout || 15000,
            clientId: opts.clientId,
            clientSecret: opts.clientSecret,
            // If username and password are provided, password auth is used.
            username: opts.username,
            password: opts.password,
            // If this gets set to true, we won't try to manage access tokens
            disableTokenManagement: opts.disableTokenManagement || false,
            // The API server sends a ping every 30 seconds
            pingTimeout: opts.pingTimeout || 35000,
        };

        this.closed = false;

        this.session = {
            ws: null,
            connection: {
                ready: false,
                task: null,
                sm: null
            },
            queue: {},
            oauth: {
                accessToken: null,
                refreshToken: null
            },
            subscriptions: {
                // id: {
                //   callback: cb,
                //   obj: obj
                // }
            }
        };

        // RPC tunnel interface
        this.rpc = new AlulaRPC(this);

        // TODO: Implement a REST tunnel, then provide a REST tunnel interface in
        // this library.
        // this.rest = new AlulaREST(this);
    }

    /**
     * @brief Publish to a channel
     */
    publish(chan, obj) {
        return trySend(this, chan, 'publish', obj);
    }

    /**
     * @brief Subscribe to a channel
     *
     * @param chan
     * @param obj
     * @param callback
     * @param [id] Optionally provide a subscription ID (usually unnecessary)
     */
    subscribe(chan, obj, callback, id) {
        let subId = id || uuid.v1();

        if (!this.session.subscriptions[subId]) {
            // Listen for subscription messages
            subscribeHandle(this, subId, callback);

            // Record subscription to resubscribe on reconnect
            this.session.subscriptions[subId] = {
                chan,
                obj,
                callback
            };
        }

        return trySend(this, chan, 'subscribe', obj, false, subId);
    }

    /**
     * @brief Send to a channel
     */
    send(chan, obj) {
        return trySend(this, chan, 'send', obj);
    }

    /**
     * @brief Not likely to be used much, but allow for auto-connection management
     * again after calling close()
     */
    open() {
        this.closed = false;
        connectionStart(this);
    }

    /**
     * @brief Close connection and suppress reconnects; call open() to undo this
     */
    close() {
        this.closed = true;
        connectionStop(this);
        this.session.queue = {};
    }

    /**
     * @brief Get current access token
     */
    getAccessToken(token) {
        return this.session.oauth.accessToken;
    }

    /**
     * @brief Allow setting the access token manually
     */
    setAccessToken(token) {
        this.session.oauth.accessToken = token;
    }
}

module.exports = AlulaAPIClient;

const trySend = (client, chan, method, obj, timeout, id) => {
    connectionStart(client);

    let fullObj = {
        channel: chan,
        id: id || uuid.v1(),
        [method]: obj
    };

    if (timeout !== false) {
        timeout = timeout || client.opts.responseTimeout;
    }

    let incomingFunc = function (resolve, reject) {
        return function incoming(inObj) {
            if (inObj.id !== fullObj.id) {
                return;
            }

            debug('found response: ' + inObj.id);

            // Clear queued item
            delete client.session.queue[inObj.id];

            // If error, reject
            if (inObj.error) {
                reject(inObj.error);
            }
            // Otherwise, resolve
            else {
                resolve(inObj);
            }
        };
    };
    let incomingFuncInst;

    let ret = new Promise((resolve, reject) => {
        incomingFuncInst = incomingFunc(resolve, reject);
        // Add event listener
        client.on('incoming', incomingFuncInst);
    });

    if (timeout !== false) {
        ret = ret.timeout(timeout)
            .catch(Promise.TimeoutError, e => {
                // Clear queued item
                delete client.session.queue[fullObj.id];

                // Reject if request times out, rather than resolving with null.
                return Promise.reject({
                    error: {
                        message: 'Request timed out',
                    },
                });
            });
    }

    ret = ret.finally(() => {
        // Remove event listener
        client.removeListener('incoming', incomingFuncInst);
    });

    trySendLower(client, fullObj, timeout);

    return ret;
};

const trySendLower = (client, fullObj, timeout) => {
    // We always add to the queue. We won't pull an item from the queue until
    // we've received a response or hit the response timeout.
    queueAdd(client, fullObj, timeout);

    if (getReady(client)) {
        try {
            debug('sending: ', fullObj);
            client.session.ws.send(JSON.stringify(fullObj));
            client.emit('send', fullObj);
        }
        catch (err) {
            client.emit('error', err);
            connectionRestart(client);
        }
    }
};

const getReady = client => {
    return client.session.connection.ready;
};

const setReady = (client, ready) => {
    client.session.connection.ready = ready;
};

const tryAuth = client => {
    let ret;

    if (client.session.oauth.accessToken) {
        // Looks like we already have an access token
        ret = Promise.resolve();
    }
    else if (client.session.oauth.refreshToken) {
        debug('get token using refresh token');

        // Get new access token with refresh token
        ret = rp({
            method: 'POST',
            uri: client.opts.authPath + '/token',
            form: {
                grant_type: 'refresh_token',
                client_id: client.opts.clientId,
                client_secret: client.opts.clientSecret,
                refresh_token: client.session.oauth.refreshToken
            },
            json: true
        })
            .then(res => {
                debug('token result: ', res);
                // Store access token
                client.session.oauth.accessToken = res.access_token;
                client.session.oauth.refreshToken = res.refresh_token;
                return Promise.resolve();
            })
            .catch(err => {
                client.session.oauth.refreshToken = null;
                return Promise.reject(err);
            });
    }
    else if (client.opts.disableTokenManagement) {
        // If the caller has disabled token management then we cannot continue.
        // Shut it all down.
        client.close();
        return Promise.reject(new Error('token management disabled'));
    }
    else {
        let formObj = {
            client_id: client.opts.clientId,
            client_secret: client.opts.clientSecret,
        };

        if (client.opts.username && client.opts.password) {
            debug('get token using password');
            formObj.grant_type = 'password';
            formObj.username = client.opts.username;
            formObj.password = client.opts.password;
        }
        else {
            debug('get token using client credentials');
            formObj.grant_type = 'client_credentials';
        }

        // Get new access and refresh tokens
        ret = rp({
            method: 'POST',
            uri: client.opts.authPath + '/token',
            form: formObj,
            json: true
        })
            .then(res => {
                debug('token result: ', res);

                client.session.oauth.accessToken = res.access_token;
                client.session.oauth.refreshToken = res.refresh_token;
                return Promise.resolve();
            });
    }

    return ret.timeout(client.opts.responseTimeout);
};

const tryConnect = client => {
    // Authenticate
    let fullPath = client.opts.path +
        '?access_token=' + client.session.oauth.accessToken;

    return new Promise((resolve, reject) => {
        // Connect
        debug('attempting connection to: ' + fullPath);
        client.session.ws = new WebSocket(fullPath);

        client.session.ws.on('message', data => {
            let obj;

            try {
                obj = JSON.parse(data);
            }
            catch (err) {
                client.emit('error', err);
                return;
            }

            debug('incoming: ', obj);
            client.emit('incoming', obj);

            // Handle messages
            switch (obj.channel) {
                case '*':
                    // READY message
                    if (obj.message && obj.message === 'ready') {
                        setReady(client, true);

                        // Resubscribe (if this is a reconn.)
                        subscribeAgain(client)
                            .then(() => {
                                // Send all queued messages
                                queueProcess(client);

                                client.emit('open');
                                resolve();
                            });
                    }
                    break;
                case 'internal.auth':
                    if (obj.event) {
                        switch (obj.event.name) {
                            case 'accessTokenExpired':
                                debug('access token expired; attempting to replace');
                                client.session.oauth.accessToken = null;

                                tryAuth(client)
                                    .then(() => {
                                        return client.send('internal.auth', {
                                            accessToken: client.session.oauth.accessToken
                                        });
                                    })
                                    .catch(err => {
                                        debug('auth/access token replacement failed', err);
                                        client.session.oauth.accessToken = null;
                                        client.session.oauth.refreshToken = null;
                                        connectionRestart(client);
                                    });
                                break;
                            case 'accessTokenRefreshed':
                                debug('replacement access token accepted');
                                break;
                        }
                    }
                    break;
            }
        });

        client.session.ws.on('error', e => {
            client.session.ws = null;
            client.session.oauth.accessToken = null;

            debug('connection error: ', e);
            client.emit('error', e);

            if (getReady(client)) {
                connectionRestart(client);
            }
            else {
                reject(e);
            }
        });

        client.session.ws.on('close', () => {
            client.session.ws = null;
            client.session.oauth.accessToken = null;

            if (getReady(client)) {
                debug('connection closed, emitting close');
                client.emit('close');
                connectionRestart(client);
            }
            else if (client.closed) {
                debug('caller initiated close');
                client.emit('close');
            }
            else {
                debug('connection closed, rejecting');
                reject();
            }
        });
    })
        .timeout(client.opts.connectTimeout);
};

const queueAdd = (client, obj, timeout) => {
    debug('adding to queue: ', obj);
    client.session.queue[obj.id] = {
        added: Date.now(),
        timeout: timeout || false, // Never times out
        obj
    };

    // Prune timed-out items
    queuePrune(client);
};

const queuePrune = client => {
    for (let k in client.session.queue) {
        let q = client.session.queue[k];
        if (q.timeout && Date.now() - q.added >= q.timeout) {
            // This item has past its lifetime
            delete client.session.queue[k];
        }
    }
};

const queueProcess = client => {
    debug('processing queue: ' + Object.keys(client.session.queue).length + ' items');
    queuePrune(client);

    for (let k in client.session.queue) {
        trySendLower(client, client.session.queue[k].obj);
    }
};

const subscribeAgain = client => {
    let subs = client.session.subscriptions;
    let p = [];

    for (let id in subs) {
        p.push(client.subscribe(subs[id].chan, subs[id].obj, subs[id].callback, id));
    }

    return Promise.all(p);
};

const subscribeHandle = (client, id, callback) => {
    client.on('incoming', inObj => {
        if (inObj.id !== id) {
            return;
        }

        try {
            callback(inObj);
        }
        catch (err) {
            debug('subscribe callback error: ', err);
        }
    });
};

const connectionStateSet = (client, state) => {
    if (!client.session.connection.sm) {
        return;
    }

    client.session.connection.sm[state]();
};

const connectionStart = client => {
    if (client.session.connection.sm) {
        return;
    }

    if (client.closed) {
        // Caller has closed
        return;
    }

    client.session.connection.sm = new StateMachine({
        init: 'begin',
        transitions: [{
            name: 'reset',
            from: '*',
            to: 'begin'
        }, {
            name: 'auth',
            from: 'begin',
            to: 'authWait'
        }, {
            name: 'connect',
            from: 'authWait',
            to: 'connectWait'
        }, {
            name: 'idle',
            from: 'connectWait',
            to: 'idleWait'
        }],
        methods: {
            onAuth: () => {
                debug('sm auth');

                if (client.closed) {
                    // The caller closed
                    return;
                }

                // Try to authenticate
                tryAuth(client)
                    .then(() => {
                        // Successful
                        connectionStateSet(client, 'connect');
                    })
                    .catch(err => {
                        // Unsuccessful, go back to 'start' state
                        connectionStateSet(client, 'reset');
                    });
            },
            onConnect: () => {
                debug('sm connect');

                if (client.closed) {
                    // The caller closed
                    return;
                }

                // Try to connect
                tryConnect(client)
                    .then(() => {
                        // Successful
                        connectionStateSet(client, 'idle');
                    })
                    .catch(err => {
                        // Reset
                        connectionStateSet(client, 'reset');
                    });
            },
            onIdle: () => {
                debug('sm idle');

                let heartbeat = function () {
                    debug('heartbeat');
                    clearTimeout(this.pingTimeout);

                    if (client.closed) {
                        return;
                    }

                    this.pingTimeout = setTimeout(() => {
                        debug('ping timeout');
                        try {
                            this.terminate();
                        }
                        catch (e) { }
                    }, client.opts.pingTimeout);
                };

                // Install ping timeout timer
                client.session.ws.on('ping', heartbeat);
                client.session.ws.on('close', function () {
                    debug('remove ping timeout');
                    clearTimeout(this.pingTimeout);
                });
                let func = heartbeat.bind(client.session.ws);
                func();
            },
            onReset: () => {
                debug('sm reset');

                if (client.closed) {
                    // The caller closed
                    return;
                }

                setReady(client, false);

                try {
                    if (client.session.ws) {
                        client.session.ws.terminate();
                        client.session.ws = null;
                    }
                }
                catch (err) { }

                // Re-auth after reconnect timeout
                setTimeout(() => {
                    connectionStateSet(client, 'auth');
                }, client.opts.reconnectTimeout);
            },
        },
    });

    // Start the state machine
    connectionStateSet(client, 'auth');
};

const connectionStop = client => {
    if (!client.session.connection.sm) {
        return;
    }

    // Cleanup connection-related stuff
    try {
        if (client.session.ws) {
            client.session.ws.terminate();
            client.session.ws = null;
        }
    }
    catch (err) { }

    client.session.connection.sm = null;
    client.session.oauth.accessToken = null;

    setReady(client, false);
};

const connectionRestart = client => {
    connectionStop(client);
    connectionStart(client);
};