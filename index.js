const WebSocket = require("ws");
const request = require("request");
const querystring = require("querystring");
const dotenv = require('dotenv');
const fs = require("fs");
const uuid = require("uuid");
dotenv.config();


function initialize() {

    var options = {
        method: "POST",
        url: process.env.URL,
        headers: { "content-type": "application/x-www-form-urlencoded" },
        body: querystring.stringify({
            grant_type: "password",
            username: process.env.USERNAME,
            password: process.env.PASSWORD,
            client_id: process.env.CLIENT_ID,
            client_secret: process.env.CLIENT_SECRET
        })
    };

    return new Promise((resolve, reject) => {
        request.post(options, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })
}

var tokenInit = initialize();

tokenInit.then((result) => {
    console.log("Getting Access_Token!");
    process.env.ACCESS_TOKEN = result.access_token;
    process.env.REFRESH_TOKEN = result.refresh_token;
    console.log("Finished!");
    console.log(`Your Access Token is: ${process.env.ACCESS_TOKEN}`);
})
.then(() => {
    connect().then(server => {

        //var array = fs.readFileSync("file2.txt").toString().split("\n");

        id = ["2b5fccd9-6a4c-43ea-8830-c7dc3edc736f", "cdac5580-9892-11e7-b688-5d554fad50d5", "adafa6e0-c986-11e7-824a-514b70e370fe"];
        var iter = 0;

        while (iter < id.length - 1) {
            setTimeout(() => {
                server.send(`{\"channel\": \"device.helix\", \"subscribe\": {\"deviceId\": \"${array[iter]}\"}}`, () => {
                    console.log("request sent");
                });
            }, 200)

            setTimeout(() => {
                server.send(`{\"channel\": \"device.helix\", \"id\": \"123-45-67-898\", \"send\": {\"deviceId\": \"${array[iter]}\", \"cmdrsp\": \"requestMfd\", \"payload\": [{\"name\": \"locationCoordinates\"}]}}`);
            }, 400)


            try {
                setTimeout(() => {
                    server.send(`{\"channel\": \"device.helix\", \"unsubscribe\": {\"consumerTag\": \"${consumerTag}\"}}`);
                }, 3000)
                iter += 1;
            } catch (err) {

            }
            //iter += 1;
        }

        setTimeout(() => {
            server.close();
        }, 60000)
        
    })
    .catch(err => {
        console.log(err);
    });
})
.catch((err) => {
    console.log("Something went wrong");
    console.log(err);
})


function connect() {
    return new Promise(function (resolve, reject) {
        var server = new WebSocket(`${process.env.WSS}${process.env.ACCESS_TOKEN}`);
        server.onopen = function () {
            resolve(server);

            var arr = [];

            server.on("message", function incoming(data) {
                d = JSON.parse(data);
                console.log(d);

                try {
                    if(d.subscribe.deviceId){
                        deviceId = d.subscribe.deviceId;
                        //console.log(`D-ID: ${deviceId}`);
                    }
                } catch (err) {
                    //console.log("No coords Yet");
                }


                try {
                    if (d.event.data.payload){
                        lat = d.event.data.payload[0].value.latitude;
                        lon = d.event.data.payload[0].value.longitude;
                        console.log(`deviceid: ${deviceId} lat: ${lat} lon:${lon}`);
                        arr.push([deviceId, lat, lon]);
                    }
                } catch (err) {
                }

                try {
                    if (d.subscribe.consumerTag){
                        consumerTag = d.subscribe.consumerTag;
                        //console.log(`Consumer Tag: ${consumerTag}`);
                    }
                } catch (err) {
                    
                }

            });

        };
        server.onerror = function (err) {
            reject(err);
        };

        server.onclose = function() {
            console.log("Connection closed");
        }

    });
}